import Vue from 'vue'
import VueRouter from 'vue-router'
import layout from '@/components/layout'

import Login from '../page/login/login.vue'

//角色模块
import gameRole from '../page/gameRole/GameRole.vue'
import skill from '../page/gameRole/Skill.vue'
import skillCanva from '../page/gameRole/SkillCanva.vue'

//商品模块
import goodsCate from '../page/goods/GoodsCate.vue'
import goods from '../page/goods/Goods.vue'

//用户管理模块
import user from '../page/user/User.vue'
import role from '../page/user/Role.vue'
import jurisdiction from '../page/user/Jurisdiction.vue'

//系统管理模块
import dataDictionary from '../page/system/DataDictionary.vue'
import loginPool from '../page/system/LoginPool.vue'

//房间管理模块
import div from '../page/room/Div.vue'
import room from '../page/room/Room.vue'

Vue.use(VueRouter)
const routes = [

  
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/skillCanva',
        component: skillCanva,
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/room',
        component: room
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/div',
        component: div
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/loginPool',
        component: loginPool
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/dataDictionary',
        component: dataDictionary
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/skill',
        component: skill
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/jurisdiction',
        component: jurisdiction
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/user',
        component: user
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/role',
        component: role
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/gameRole',
        component: gameRole
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/goodsCate',
        component: goodsCate
      }
    ]
  },
  {
    path: '/layout',
    component: layout,
    children: [
      {
        path: '/goods',
        component: goods
      }
    ]
  },
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
