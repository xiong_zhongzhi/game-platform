import axios from 'axios'
import { Message, MessageBox } from 'element-ui'  //导入element-ui组件库
// 创建axios的对象
const instance = axios.create({
    baseURL: "http://localhost:9001/",  //配置固定域名
    timeout: 5000,
    withCredentials: true
})

// 请求拦截
// 所有的网络请求都会走这个方法,可以在请求添加自定义内容
instance.interceptors.request.use(
    function (config) {
        const token = localStorage.getItem('token');
        token && (config.headers.Authorization = "Bearer "+token)
        return config
    },
    function (err) {
        return Promise.request(err)
    }
)

// 响应拦截
// 此处可以根据服务器返回的状态码做相应的数据
instance.interceptors.response.use(
    function (response) {
        const res = response
        if (res.status === 500) {
        } else if (res.errno === 405) {
        }
        if(response.data.state==-2){
            MessageBox.alert('请重新登录',response.data.msg , {
                confirmButtonText: '确定',
                type: 'error'
            })
            window.location.href = "/";
        }
        if(response.data.state==-3){
            MessageBox.alert(response.data.msg,"错误" , {
                confirmButtonText: '确定',
                type: 'error'
            })
        }
        if(response.data.state==-4){
            MessageBox.alert(response.data.msg,"错误" , {
                confirmButtonText: '确定',
                type: 'error'
            })
        }
        return response.data;
    },
    function (err) {
        return Promise.request(err)
    }
)

// 封装get和post请求
export function get(url, params) {
    return instance.get(url, {params})
}
 
export function post(url, data) {
    return instance.post(url, data)
}
 
export default instance;