export default class MapContainer {
    constructor() {
      this.container = {};
    }
  
    set(key, value) {
      this.container[key] = value;
    }
  
    get(key) {
      return this.container[key];
    }
  
    has(key) {
      return key in this.container;
    }
  
    delete(key) {
      delete this.container[key];
    }
  
    clear() {
      this.container = {};
    }
  
    keys() {
      return Object.keys(this.container);
    }
  
    values() {
      return Object.values(this.container);
    }
  
    entries() {
      return Object.entries(this.container);
    }
  }