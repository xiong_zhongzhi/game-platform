import request from "@/util/request";  //导入封装请求的js文件

import { Message, MessageBox } from 'element-ui'  //导入element-ui组件库
 
export function getRequest(url,params) {
  return request({
    url,  //接口路径
    method: "get",  //接口方法
    headers: { 'Content-Type': 'multipart/form-data' }, //给接口添加请求头
    params  //接口参数
  });
}

export function putRequest(url,data) {
  return request({
    url,
    method: "put",
    headers: {
      "Content-Type": "application/json",
    },
    data:data,
  });  
}

export function deleteRequest(url,data) {
  return request({
    url,
    method: "delete",
    headers: {
      "Content-Type": "application/json",
    },
    data:data
  });  
}

export function postRequest(url,data) {
  return request({
    url,
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    data:data
  });  
}
